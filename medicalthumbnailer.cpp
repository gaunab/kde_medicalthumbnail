/**
 * Copyright (C) 2017 Paul Kuntke - Institut und Poliklinik für Neuroradiologie, Uniklinikum Dresden 
 * Released under LGPL license 
 *
 */

#include "medicalthumbnailer.h"
#include "nifti.h"
#include <iostream>


Q_LOGGING_CATEGORY(LOG_MEDICALTHUMB, "MediThumbCreator")


extern "C"
{
    Q_DECL_EXPORT ThumbCreator *new_creator()
    {
        return new MediThumbCreator();
    }
}

MediThumbCreator::MediThumbCreator() {

}

MediThumbCreator::~MediThumbCreator() {

}



bool MediThumbCreator::create(const QString &path, int width, int height, QImage &img)
{

  qCDebug(LOG_MEDICALTHUMB) << "Medical thumbnail generation for: " << path;

  std::string s_path = path.toStdString();
  char * c_path = new char[s_path.size()+1];
  strcpy(c_path, s_path.c_str());
  QImage timg;

  try {
	  timg = createThumbnail(c_path).scaled(width,height, Qt::KeepAspectRatio);
	  img = timg;
	}
  catch (int e)  {
  qCWarning(LOG_MEDICALTHUMB) << "[medical thumbnailer] exception:" << e;
  return None;
  }
  return !img.isNull();
}

ThumbCreator::Flags MediThumbCreator::flags() const {
    return DrawFrame;
}
