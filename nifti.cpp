#include "nifti.h"


ImageType2D::Pointer extract2DImageSlice(ImageType3D::Pointer reader, int plane) {

    FilterType2D::Pointer filter = FilterType2D::New();


    // Calculate Center
    ImageType3D::RegionType inputRegion = reader->GetLargestPossibleRegion(); // read ImageDimension
    ImageType3D::SizeType size = inputRegion.GetSize();                       // extact actual size
    unsigned int slice = 0;                                                    
    // if (size[plane] > 0) {                                                         // fetch slice from center
    //    slice = size[plane] / 2;                                                    
    // }
    //
    // Calculate Center of Gravity 
    ImageCalculatorType::Pointer imageCalculator = ImageCalculatorType::New();  
    imageCalculator->SetImage(reader);
    imageCalculator->Compute();

    ImageCalculatorType::VectorType gravityCenter = imageCalculator->GetCenterOfGravity();
    ImageType3D::IndexType centerPix;
    typedef itk::Point< float, ImageType3D::ImageDimension > PointType;
    PointType centerPhys;
    centerPhys[0] = gravityCenter[0];
    centerPhys[1] = gravityCenter[1];
    centerPhys[2] = gravityCenter[2];
    

    const bool isInside = reader->TransformPhysicalPointToIndex( centerPhys, centerPix );
    slice = centerPix[plane];
    if (!isInside) {                    // center of Gravity is calculated outside of Image - fall back to center of image
        slice = 0;
        if (size[plane] > 0) {                                                         // fetch slice from center
           slice = size[plane] / 2;                                                    
        }
    }
    std::cout << centerPhys << centerPix << std::endl;
    qDebug() << "Using slice "<< slice;
    size[plane] = 0;                                                               // set size of desired direction to single Slice
    ImageType3D::IndexType start = inputRegion.GetIndex();                    // read start-indices 
    start[plane] = slice;                                                          // set start-index for desired plane
	qDebug() << "got start index for plane " << plane;
    ImageType3D::RegionType desiredRegion;
    desiredRegion.SetSize(  size  );
    desiredRegion.SetIndex( start );
	qDebug() << "trying to apply region extraction filter"; 
    filter->SetExtractionRegion( desiredRegion );
    filter->SetInput( reader );
	filter->SetDirectionCollapseToSubmatrix();

	qDebug() << "create a new image from filter";
    ImageType2D::Pointer img = filter->GetOutput();
	qDebug() << "updating img";
    img->Update();
	qDebug() << "returning img";
    return img;
}





QImage createThumbnail(char* path) {
    ReaderType::Pointer reader = ReaderType::New();

    reader->SetFileName( path );
    reader->Update();
    ImageType3D::Pointer image = reader->GetOutput();

    StatisticsFilterType::Pointer statistics1 = StatisticsFilterType::New();
    statistics1->SetInput(image);
    statistics1->Update();

    const unsigned int numberOfComponents = 2;                                                  // Components for Histogram
    HistogramGeneratorType::Pointer histogramGenerator = HistogramGeneratorType::New();
    histogramGenerator->SetInput(  reader->GetOutput() );
    histogramGenerator->SetNumberOfBins( 256 );
    histogramGenerator->SetMarginalScale( 10.0 );
    histogramGenerator->SetHistogramMin( statistics1->GetMinimum() );
    histogramGenerator->SetHistogramMax( statistics1->GetMaximum() );
    histogramGenerator->Compute();
    
    const HistogramType * histogram = histogramGenerator->GetOutput(); 
    

    ThresholdImageFilterType::Pointer thresholdFilter = ThresholdImageFilterType::New();
    thresholdFilter->SetInput(image);
    thresholdFilter->ThresholdAbove( histogram->Quantile(0,0.99) );
    thresholdFilter->SetOutsideValue( histogram->Quantile(0,0.99) );
  
    std::cout << "99. Percentile: " << histogram->Quantile(0,0.99) << std::endl; 
  // Rescale intensity of image
  RescaleFilterType::Pointer rescaleFilter = RescaleFilterType::New();
  rescaleFilter->SetInput( thresholdFilter->GetOutput());
  rescaleFilter->SetOutputMinimum(0);
  rescaleFilter->SetOutputMaximum(255);
  rescaleFilter->Update();
  ImageType3D::Pointer rescaledImage = rescaleFilter->GetOutput();



  ImageType3D::RegionType region = image->GetLargestPossibleRegion();
  ImageType3D::SizeType size = region.GetSize();

  // Now extract the Image in each direction
  ImageType2D::Pointer singleSlice0 = extract2DImageSlice(rescaledImage, 0); 
  ImageType2D::Pointer singleSlice1 = extract2DImageSlice(rescaledImage, 1); 
  ImageType2D::Pointer singleSlice2 = extract2DImageSlice(rescaledImage, 2); 

  /*
  std::cout << " Mean: " << statistics1->GetMean()  << " Variance: " << statistics1->GetSigma() << std::endl;
  std::cout << " Min: " << statistics1->GetMinimum() << " Max: " << statistics1->GetMaximum() << std::endl;
  */

  int pixelValue;
  // int min = 1000;
  // int max = 0;
  ImageType2D::RegionType sliceRegion0  = singleSlice0->GetLargestPossibleRegion();
  ImageType2D::SizeType sliceSize0      = sliceRegion0.GetSize();
  ImageType2D::RegionType sliceRegion1  = singleSlice1->GetLargestPossibleRegion();
  ImageType2D::SizeType sliceSize1      = sliceRegion1.GetSize();
  ImageType2D::RegionType sliceRegion2  = singleSlice2->GetLargestPossibleRegion();
  ImageType2D::SizeType sliceSize2      = sliceRegion2.GetSize();
  
  QImage img = QImage(sliceSize0[0] + sliceSize1[0], sliceSize1[1] + sliceSize2[1],QImage::Format_RGB32);
  // Image 0
  for (unsigned int x = 0; x < sliceSize0[0]; x++) {
      for (unsigned int y = 0; y < sliceSize0[1]; y++) {
          pixelValue = singleSlice0->GetPixel({{x,y}}); 
          img.setPixel(x,y, qRgb(pixelValue, pixelValue, pixelValue));
      }
  }
  // Image 1
  for (unsigned int x = 0; x < sliceSize1[0]; x++) {
      for (unsigned int y = 0; y < sliceSize1[1]; y++) {
          pixelValue = singleSlice1->GetPixel({{x,y}}); 
          img.setPixel(sliceSize0[0]+ x,y, qRgb(pixelValue, pixelValue, pixelValue));
      }
  }
  // Image 2
  for (unsigned int x = 0; x < sliceSize2[0]; x++) {
      for (unsigned int y = 0; y < sliceSize2[1]; y++) {
          pixelValue = singleSlice2->GetPixel({{x,y}}); 
          img.setPixel(sliceSize0[0] + x,sliceSize0[1] + y, qRgb(pixelValue, pixelValue, pixelValue));
      }
  }

  // Black area
  for (unsigned int x = 0; x < sliceSize2[0]; x++) {
      for (unsigned int y = 0; y < sliceSize2[1]; y++) {
          img.setPixel(x,sliceSize0[1] + y, qRgb(0,0,0));
      }
  }

  return img;

}

