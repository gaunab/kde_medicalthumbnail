#include "niftilib_test.h"

int main(int argc, char **argv) {

    std::cout << argc << std::endl;
    ReaderType::Pointer reader = ReaderType::New();
    
    reader->SetFileName( argv[1] );
    reader->Update();

    ImageType3D::Pointer image = reader->GetOutput();
    ImageType3D::RegionType region = image->GetLargestPossibleRegion();
    ImageType3D::SizeType size = region.GetSize();                       // extact actual size

    StatisticsFilterType::Pointer statistics1 = StatisticsFilterType::New();
    statistics1->SetInput(reader->GetOutput());
    statistics1->Update();
    std::cout << "Size: " << size << std::endl;

    std::cout << " Mean: " << statistics1->GetMean()  << " Variance: " << statistics1->GetSigma() << std::endl;
    std::cout << " Min: " << statistics1->GetMinimum() << " Max: " << statistics1->GetMaximum() << std::endl;

    return EXIT_SUCCESS;
}
