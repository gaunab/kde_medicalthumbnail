/**
 * Copyright (C) 2017 Paul Kuntke - Institut und Poliklinik für Neuroradiologie, Uniklinikum Dresden 
 * Released under LGPL license 
 *
 */

#ifndef MEDITHUMBNAIL_H
#define MEDITHUMBNAIL_H

#include <QtCore/QObject>
#include <kio/thumbcreator.h>
#include <QLoggingCategory>
Q_DECLARE_LOGGING_CATEGORY(LOG_MEDICALTHUMB)

class MediThumbCreator : public QObject, public ThumbCreator {

    Q_OBJECT

    public:
        explicit MediThumbCreator();
        virtual ~MediThumbCreator();
        virtual bool create( const QString &path, int width, int height, QImage &img);
        virtual Flags flags() const;
};

#endif
