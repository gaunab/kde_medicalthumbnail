/**
 * Copyright (C) 2017 Paul Kuntke - Institut und Poliklinik für Neuroradiologie, Uniklinikum Dresden 
 * Released under LGPL license 
 *
 */

#include "thumb.h"
#include "nifti.h"

int main(int argc, char **argv) {

    QGuiApplication a(argc, argv);
    QImage img;
    
    if (argc > 0) { 
        img = createThumbnail( argv[1] );
    }

    QLabel myLabel;
    myLabel.setPixmap(QPixmap::fromImage(img));

    myLabel.show();

    return a.exec();

}

