/**
 * Copyright (C) 2017 Paul Kuntke - Institut und Poliklinik für Neuroradiologie, Uniklinikum Dresden 
 * Released under LGPL license 
 *
 */

#include "medicalextractor.h"
#include <QDebug>
#include <QFile>
#include <fstream>
// #include "KF5/KFileMetaData/kfilemetadata/extractorplugin.h"


using namespace KFileMetaData;


MedicalExtractor::MedicalExtractor(QObject* parent)
	: ExtractorPlugin(parent) 
{
}

QStringList MedicalExtractor::mimetypes() const {

    QStringList list;
    
    list << QStringLiteral("image/nifti");
	list << QStringLiteral("application/dicom");

    return list;

    
}

 
void MedicalExtractor::extract(ExtractionResult* result) {
}

