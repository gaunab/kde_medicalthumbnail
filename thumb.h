/**
 * Copyright (C) 2017 Paul Kuntke - Institut und Poliklinik für Neuroradiologie, Uniklinikum Dresden 
 * Released under LGPL license 
 *
 */

#ifndef THUMB_H
#define THUMB_H 
#include "nifti.h"
#include <iostream>
#include <QtGui/QImage>
#include <QGuiApplication>
#include <QLabel>

#endif // THUMB_H
