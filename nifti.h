#ifndef NIFTITHUMB_H
#define NIFTITHUMB_H

#include "itkImageAdaptor.h"
// #include "itkQtAdaptor.h"
#include "itkImage.h"
#include "itkImageFileReader.h"
#include <itkImageFileWriter.h>
#include <itkExtractImageFilter.h>
#include "itkNormalizeImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkThresholdImageFilter.h"
#include "itkHistogram.h"
#include "itkScalarImageToHistogramGenerator.h"
#include "itkLabelStatisticsImageFilter.h"
#include <itkIntensityWindowingImageFilter.h>
#include <itkImageMomentsCalculator.h>

#include <itkCastImageFilter.h>
#include <QtGui/QImage>
#include <QtCore/QDebug>

typedef float         PixelType;
typedef unsigned int        OutputPixelType2D;
typedef itk::Image< PixelType, 3 > ImageType3D;
typedef itk::Image< PixelType, 2 >  ImageType2D;
typedef itk::Image< OutputPixelType2D, 2 >    OutputImageType2D;
typedef itk::ImageFileReader< ImageType3D >  ReaderType;
typedef itk::ImageFileWriter< OutputImageType2D > WriterType;
typedef itk::ExtractImageFilter< ImageType3D, ImageType2D > FilterType2D;
typedef itk::CastImageFilter <ImageType2D, OutputImageType2D> FilterTypeFtoI;
typedef itk::NormalizeImageFilter< ImageType3D, ImageType3D > NormalizeFilterType;
typedef itk::StatisticsImageFilter< ImageType3D >   StatisticsFilterType;
typedef itk::RescaleIntensityImageFilter< ImageType3D, ImageType3D > RescaleFilterType;
typedef itk::ImageRegionIteratorWithIndex<ImageType2D> IteratorType;
typedef itk::ThresholdImageFilter <ImageType3D>  ThresholdImageFilterType;
typedef itk::IntensityWindowingImageFilter <ImageType3D, ImageType3D > WindowFilterType;
typedef itk::Statistics::ScalarImageToHistogramGenerator< ImageType3D >   HistogramGeneratorType;
typedef HistogramGeneratorType::HistogramType  HistogramType;
// to calculate centerOfGravity:
typedef itk::ImageMomentsCalculator<ImageType3D> ImageCalculatorType;



ImageType2D::Pointer extract2DImageSlice(ImageType3D::Pointer reader, int plane);
QImage createThumbnail(char* path);

#endif
