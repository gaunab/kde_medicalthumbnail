#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkLabelStatisticsImageFilter.h"
#include <itkStatisticsImageFilter.h>
#include <iostream>


typedef float                                   PixelType;
typedef unsigned int                            OutputPixelType2D;
typedef itk::Image< PixelType, 3 >              ImageType3D;
typedef itk::ImageFileReader< ImageType3D >     ReaderType;
typedef itk::StatisticsImageFilter< ImageType3D >   StatisticsFilterType;

