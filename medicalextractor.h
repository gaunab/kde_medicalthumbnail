/**
 * Copyright (C) 2017 Paul Kuntke - Institut und Poliklinik für Neuroradiologie, Uniklinikum Dresden 
 * Released under LGPL license 
 *
 */

#ifndef MEDICALEXTRACTOR_H
#define MEDICALEXTRACTOR_H

// #include "extractorplugin.h"
#include <KF5/KFileMetaData/kfilemetadata/extractorplugin.h>
// #include <extractorplugin.h>
/* #include "itkImageFileReader.h"
#include "itkLabelStatisticsImageFilter.h"

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkLabelStatisticsImageFilter.h"
#include <itkStatisticsImageFilter.h> */
// #include <iostream>
#include <QTextStream>
#include <QDebug>
/*
typedef float                                   PixelType;
typedef itk::Image< PixelType, 3 >              ImageType3D;
typedef itk::ImageFileReader< ImageType3D >     ReaderType;
typedef itk::StatisticsImageFilter< ImageType3D >   StatisticsFilterType;

*/

namespace KFileMetaData {

        class MedicalExtractor : public ExtractorPlugin {

             Q_OBJECT
             Q_PLUGIN_METADATA(IID "org.kde.kf5.kfilemetadata.ExtractorPlugin")
             Q_INTERFACES(KFileMetaData::ExtractorPlugin)

            public:
                MedicalExtractor(QObject* parent=0);

                void extract(ExtractionResult* result) Q_DECL_OVERRIDE;
                QStringList mimetypes() const Q_DECL_OVERRIDE;


        };
}

#endif // MEDICALEXTRACTOR_H 
