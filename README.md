# README #
This is a KDE thumbnailgenerator for medical image data formats. Currently this is the KDE5 version.

# Requires

* insight tool kit (https://itk.org) 
    debian: libinsighttoolkit4-dev
* cMake

# Installation

To compile and install open the source directory and run the following commands.

    mkdir build
    cd build`
    cmake ../
    make
    sudo make install
    
# KfileMetadata

    libkf5filemetadata-dev

To extract file informations a kFileMetaData-Plugin is used:
[KFileMetadata API](https://api.kde.org/frameworks/kfilemetadata/html/index.html)



=======

# TODO
* Only use a single pane for 2D-Images
* KfileMetaDataPlugin for KDE5 is not working yet
* make install does'nt choose the right paths yet
